(defun efs/run-in-background (command)
  (let ((command-parts (split-string command "[ ]+")))
    (apply #'call-process `(,(car command-parts) nil 0 nil ,@(cdr command-parts)))))

(defun efs/set-wallpaper ()
  (interactive)
  ;; NOTE: You will need to update this to a valid background path!
  (start-process-shell-command
      "nitrogen" nil  "nitrogen --restore"))

(defun efs/exwm-init-hook ()
  ;; Make workspace 1 be the one where we land at startup
  (exwm-workspace-switch-create 1)

  ;; Open eshell by default
  ;;(eshell)

  ;; Show battery status in the mode line
  (display-battery-mode 1)

  ;; Show the time and date in modeline
  (setq display-time-day-and-date t)
  (display-time-mode 1)
  ;; Also take a look at display-time-format and format-time-string

  ;; Launch apps that will run in the background
  (efs/run-in-background "nm-applet")
  (efs/run-in-background "pasystray")
  (eft/run-in-background "volumeicon")
  (efs/run-in-background "blueman-applet"))

(defun efs/exwm-update-class ()
  (exwm-workspace-rename-buffer exwm-class-name))

(defun efs/exwm-update-title ()
  (pcase exwm-class-name
    ("Firefox" (exwm-workspace-rename-buffer (format "Firefox: %s" exwm-title)))))

;; This function isn't currently used, only serves as an example how to
;; position a window
(defun efs/position-window ()
  (let* ((pos (frame-position))
         (pos-x (car pos))
          (pos-y (cdr pos)))

    (exwm-floating-move (- pos-x) (- pos-y))))

(defun efs/configure-window-by-class ()
  (interactive)
  (pcase exwm-class-name
    ("Firefox" (exwm-workspace-move-window 2))
    ("Sol" (exwm-workspace-move-window 3))
    ("mpv" (exwm-floating-toggle-floating)
           (exwm-layout-toggle-mode-line))))

(use-package exwm
  :config
  (require 'exwm-config)
  (exwm-config-default)
  ;; Set the default number of workspaces
  (setq exwm-workspace-number 10)

  ;; TODO: Add multi-monitor for desktop
  ;; When window "class" updates, use it to set the buffer name
  (add-hook 'exwm-update-class-hook #'efs/exwm-update-class)

  ;; When window title updates, use it to set the buffer name
  (add-hook 'exwm-update-title-hook #'efs/exwm-update-title)

  ;; Configure windows as they're created
  (add-hook 'exwm-manage-finish-hook #'efs/configure-window-by-class)

  ;; When EXWM starts up, do some extra confifuration
  (add-hook 'exwm-init-hook #'efs/exwm-init-hook)

  ;; Rebind CapsLock to Ctrl
  (start-process-shell-command "xmodmap" nil "xmodmap ~/.emacs.d/exwm/Xmodmap")

  ;; NOTE: Uncomment the following two options if you want window buffers
  ;;       to be available on all workspaces!

  ;; Automatically move EXWM buffer to current workspace when selected
  (setq exwm-layout-show-all-buffers t)

  ;; Display all EXWM buffers in every workspace buffer list
  (setq exwm-workspace-show-all-buffers t)

  ;; NOTE: Uncomment this option if you want to detach the minibuffer!
  ;; Detach the minibuffer (show it with exwm-workspace-toggle-minibuffer)
  ;;(setq exwm-workspace-minibuffer-position 'top)

  ;; Set the screen resolution (update this to be the correct resolution for your screen!)
  (require 'exwm-randr)
  (exwm-randr-workspace-output-plist '(0 "LSVD1"))

  ;; X220
  (add-hook 'exwm-randr-screen-change-hook
        (lambda ()
  (start-process-shell-command "xrandr" nil "xrandr --output LVDS1 --primary --mode 1366x768 --pos 0x0 --rotate normal")))
  ;; Desktop
  ;; (add-hook 'exwm-randr-screen-change-hook
  ;;           (lambda ()
  ;;             (start-process-shell-command
  ;;               "xrandr" nil "xrandr --output DVI-D-0 --primary --mode 1920x1080 --pos 0x0 --rotate normal --rate 144.00
  ;;                                    --output HDMI-0 --mode 1920x1080 --pos 1920x0 --rotate normal --rate 60.00")))
  (exwm-randr-enable)

  ;; Set the wallpaper after changing the resolution
  (efs/set-wallpaper)

  ;; Load the system tray before exwm-init
  (require 'exwm-systemtray)
  (setq exwm-systemtray-height 32)
  (exwm-systemtray-enable)

  ;; These keys should always pass through to Emacs
  (setq exwm-input-prefix-keys
    '(?\C-x
      ?\C-u
      ?\C-h
      ?\M-x
      ?\M-`
      ?\M-&
      ?\M-:
      ?\C-\M-j  ;; Buffer list
      ?\C-\ ))  ;; Ctrl+Space

  ;; Ctrl+Q will enable the next key to be sent directly
  (define-key exwm-mode-map [?\C-q] 'exwm-input-send-next-key)

  (setq exwm-input-simulation-keys
        '(([?\C-b] . [left])
          ([?\C-f] . [right])
          ([?\C-p] . [up])
          ([?\C-n] . [down])
          ([?\C-a] . [home])
          ([?\C-e] . [end])
          ([?\M-v] . [prior])
          ([?\C-v] . [next])
          ([?\C-d] . [delete])
          ([?\C-k] . [S-end delete])))

  ;; Set up global key bindings.  These always work, no matter the input state!
  ;; Keep in mind that changing this list after EXWM initializes has no effect.
  (setq exwm-input-global-keys
        `(
          ;; Reset to line-mode (C-c C-k switches to char-mode via exwm-input-release-keyboard)
          ([?\s-r] . exwm-reset)

          ;; Toggle commands
          ([?\s-f] . exwm-floating-toggle-floating)
          ([?\s-m] . exwm-layout-toggle-mode-line)
          ([f11] . exwm-layout-toggle-fullscreen)

          ;; managing workspaces
          ([?\s-w] . exwm-workspace-switch)
          ([?\s-W] . exwm-workspace-swap)
          ([?\s-\C-w] . exwm-workspace-move)
          ;; essential programs
          ([?\s-d] . dired)
          ([?\s-e] . eshell)
          ;; ([s-return] . dt/exwm-start-alacritty-with-fish)
          ;; ([s-S-return] . dmenu)
          ;; killing buffers and windows
          ([?\s-b] . ibuffer)
          ([?\s-B] . kill-current-buffer)
          ([?\s-C] . +workspace/close-window-or-workspace)

          ;; Move between windows
          ([s-left] . windmove-left)
          ([s-right] . windmove-right)
          ([s-up] . windmove-up)
          ([s-down] . windmove-down)

          ;; ([?\s-0] . (lambda () (interactive) (exwm-workspace-switch-create 0)))
          ;; ([?\s-1] . (lambda () (interactive) (exwm-workspace-switch-create 1)))
          ;; ([?\s-2] . (lambda () (interactive) (exwm-workspace-switch-create 2)))
          ;; ([?\s-3] . (lambda () (interactive) (exwm-workspace-switch-create 3)))
          ;; ([?\s-4] . (lambda () (interactive) (exwm-workspace-switch-create 4)))
          ;; ([?\s-5] . (lambda () (interactive) (exwm-workspace-switch-create 5)))
          ;; ([?\s-6] . (lambda () (interactive) (exwm-workspace-switch-create 6)))
          ;; ([?\s-7] . (lambda () (interactive) (exwm-workspace-switch-create 7)))
          ;; ([?\s-8] . (lambda () (interactive) (exwm-workspace-switch-create 8)))
          ;; ([?\s-9] . (lambda () (interactive) (exwm-workspace-switch-create 9)))
          ;; move window workspace with SUPER+SHIFT+{0-9}
          ([?\s-\)] . (lambda () (interactive) (exwm-workspace-move-window 0)))
          ([?\s-!] . (lambda () (interactive) (exwm-workspace-move-window 1)))
          ([?\s-@] . (lambda () (interactive) (exwm-workspace-move-window 2)))
          ([?\s-#] . (lambda () (interactive) (exwm-workspace-move-window 3)))
          ([?\s-$] . (lambda () (interactive) (exwm-workspace-move-window 4)))
          ([?\s-%] . (lambda () (interactive) (exwm-workspace-move-window 5)))
          ([?\s-^] . (lambda () (interactive) (exwm-workspace-move-window 6)))
          ([?\s-&] . (lambda () (interactive) (exwm-workspace-move-window 7)))
          ([?\s-*] . (lambda () (interactive) (exwm-workspace-move-window 8)))
          ([?\s-\(] . (lambda () (interactive) (exwm-workspace-move-window 9)))

          ;; Launch applications via shell command
          ([?\s-&] . (lambda (command)
                       (interactive (list (read-shell-command "$ ")))
                       (start-process-shell-command command nil command)))

          ;; Switch workspace
          ([?\s-w] . exwm-workspace-switch)
          ;; ([?\s-`] . (lambda () (interactive) (exwm-workspace-switch-create 0)))

          ;; 's-N': Switch to certain workspace with Super (Win) plus a number key (0 - 9)
          ,@(mapcar (lambda (i)
                      `(,(kbd (format "s-%d" i)) .
                        (lambda ()
                          (interactive)
                          (exwm-workspace-switch-create ,i))))
                    (number-sequence 0 9))))

  (exwm-input-set-key (kbd "s-SPC") 'counsel-linux-app)

  (exwm-enable))

(use-package desktop-environment
  :after exwm
  :config (desktop-environment-mode)
  :custom
  (desktop-environment-brightness-small-increment "2%+")
  (desktop-environment-brightness-small-decrement "2%-")
  (desktop-environment-brightness-normal-increment "5%+")
  (desktop-environment-brightness-normal-decrement "5%-"))
