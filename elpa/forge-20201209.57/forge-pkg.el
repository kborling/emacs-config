(define-package "forge" "20201209.57" "Access Git forges from Magit."
  '((emacs "25.1")
    (closql "1.0.0")
    (dash "2.14.1")
    (emacsql-sqlite "3.0.0")
    (ghub "20190319")
    (let-alist "1.0.5")
    (magit "20190408")
    (markdown-mode "2.3")
    (transient "0.1.0"))
  :commit "953764d2bb57b6bbaec4a2048722050fd15732db" :url "https://github.com/magit/forge")
;; Local Variables:
;; no-byte-compile: t
;; End:
