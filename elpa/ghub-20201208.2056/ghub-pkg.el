(define-package "ghub" "20201208.2056" "Minuscule client libraries for Git forge APIs."
  '((emacs "25.1")
    (let-alist "1.0.5")
    (treepy "0.1.1"))
  :commit "a72a171b3c686b98ea2b5db11ad159b0febbf545")
;; Local Variables:
;; no-byte-compile: t
;; End:
