(define-package "lsp-mode" "20201209.1825" "LSP mode"
  '((emacs "26.1")
    (dash "2.14.1")
    (dash-functional "2.14.1")
    (f "0.20.0")
    (ht "2.0")
    (spinner "1.7.3")
    (markdown-mode "2.3")
    (lv "0"))
  :commit "dbe83b69a3324a4639c6f6645bf7c7a164094b0c" :keywords
  ("languages")
  :authors
  (("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainer
  ("Vibhav Pant, Fangrui Song, Ivan Yonchovski")
  :url "https://github.com/emacs-lsp/lsp-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
