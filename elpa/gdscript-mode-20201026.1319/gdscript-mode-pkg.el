(define-package "gdscript-mode" "20201026.1319" "Major mode for Godot's GDScript language"
  '((emacs "26.3"))
  :commit "75fe658ab88adbec95f226d24fd9d41c33c68dd5" :keywords
  ("languages")
  :authors
  (("Nathan Lovato <nathan@gdquest.com>, Fabián E. Gallina" . "fgallina@gnu.org"))
  :maintainer
  (nil . "nathan@gdquest.com")
  :url "https://github.com/GDQuest/emacs-gdscript-mode/")
;; Local Variables:
;; no-byte-compile: t
;; End:
